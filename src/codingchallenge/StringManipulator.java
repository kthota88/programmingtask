package codingchallenge;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.BitSet;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

/**
 * This is this the implementation of StringManipulator class.
 */
public class StringManipulator {
	/**
	 * Main method of the application.
	 * 
	 * @param args
	 *            - Array of command line arguments
	 */
	public static void main(String[] args) {

		StringManipulator stringManipulator = new StringManipulator();

		/**
		 * Process the challenge_tests.csv file
		 */
		BufferedReader br = null;
		String line = "";
		// List to store words
		List<String> words = new LinkedList<String>();
		try {
			br = new BufferedReader(new FileReader("src/atdd/challenge_tests.csv"));

			// Skip header
			br.readLine();

			// Read lines from the file
			while ((line = br.readLine()) != null) {
				if (!line.trim().isEmpty()) {
					String word = line;
					words.add(word);
					System.out.println("Has Unique Character:" + stringManipulator.hasUniqueChars(word));
				}
			}

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			// Close buffer reader
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

		// Sort words if list is not empty
		if (!words.isEmpty()) {
			String[] arr = new String[words.size()];
			stringManipulator.sortStrings(words.toArray(arr));
		}
	}

	/**
	 * Removes all non-alphabetic characters from a string and converts all
	 * characters to upper case.
	 * 
	 * @param str
	 *            - String to clean
	 * @return Cleaned string in upper case.
	 */
	public String cleanString(String str) {
		// If string is null or empty then return str else processed.

		if (null == str || str.trim().isEmpty())
			return str;
		/*
		 * Using regex we replace non-alphabetic character to "" and then convert string
		 * to upper case.
		 */
		str = str.replaceAll("[^a-zA-Z]", "");
		str = str.toUpperCase();
		return str;
	}

	/**
	 * Check given string has unique character or not.if not then return false else
	 * true.It checks alphabetics only.If it is null or empty then it return false.
	 * 
	 * @param word
	 *            - String in which we check whether it has unique character or not.
	 *            If it has duplicate then return false then return true.
	 * @return Boolean value which indicates string has unique character
	 */
	public boolean hasUniqueChars(String word) {

		if (null == word || word.trim().isEmpty())
			return false;

		// Remove non-alphabetic character and convert it to upper case/lower case
		word = cleanString(word);

		// Create the bit set of given size of bits.Initailize all bits to false
		BitSet bs = new BitSet(1 << Character.SIZE / 2);
		bs.clear();
		for (char c : word.toCharArray()) {
			// If bit value of given character is set to true means this character is
			// duplicate and return false
			if (bs.get(c)) {
				return false;
			}
			// Set bit to true at given index(ASCII value of character)
			bs.set(c);
		}

		return true;

	}

	/**
	 * Get the average weight of passed string. Weight is numeric value that is the
	 * average of each char ASCII value contained in the string.
	 * 
	 * @param word
	 *            -Word whose weight is calculated
	 * @return Average weight of the string
	 */
	public double getWeight(String word) {
		if (null == word || word.trim().isEmpty())
			return 0;
		double weight = 0;
		word = cleanString(word);
		for (char c : word.toCharArray()) {
			weight += (int) c;
		}
		return weight / word.length();
	}

	/**
	 * Sort the Strings as per weight if array is not null and size of array is
	 * 
	 * @param words
	 *            - Array of words used to sort the Strings
	 */
	public void sortStrings(String[] words) {
		if (null == words || words.length == 0)
			return;
		// Clean strings
		for (int i = 0; i < words.length; i++) {
			if (null != words[i]) {
				words[i] = cleanString(words[i]);
			}
		}

		// Sort the array as per their weight
		Arrays.sort(words, new Comparator<String>() {
			@Override
			public int compare(String o1, String o2) {

				// Get weight and compare
				double w1 = getWeight(o1);
				double w2 = getWeight(o2);
				if (w1 < w2) {
					return -1;
				} else if (w1 > w2) {
					return 1;
				}
				return 0;
			}
		});

		// Write data to file
		BufferedWriter bw = null;
		try {
			bw = new BufferedWriter(new FileWriter("src/atdd/challenge_sorted.csv"));
			bw.write("Words,Unique,Weight\n");
			for (int i = 0; i < words.length; i++) {
				if (null != words[i]) {
					String str = words[i] + "," + (hasUniqueChars(words[i]) ? "TRUE" : "FALSE") + ","
							+ getWeight(words[i]) + "\n";
					System.out.print(str);
					bw.write(str);
				}
			}
			System.out.println("Sorted data is written to src/atdd/challenge_sorted.csv file");
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			// Flush and close buffer writer
			if (bw != null) {
				try {
					bw.flush();
					bw.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

	}
}
